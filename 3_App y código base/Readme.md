# Aplicación y su código base
Aquí se pueden encontrar el .Apk descargable para el control de la incubadora y el .iai para poder ver el código de la App.
Si el objetivo sólo es hacer uso de la aplicación, no es necesario descargarse el .iai, ya que éste solo sirve para quienes quieran ver y/o editar el código que controla a la aplicación. 
Una vez descargada el .apk se debe instalar la aplicación en el móvil. Como recomendación, es preferible descargar el .apk directamente al móvil y desde la carpeta de "descargas" del mismo, hacer la instalación. 

Las instrucciones para usar la aplicación se pueden ver en el Manual de Armado y Operación en el apartado de Operación.


