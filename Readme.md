# INCUBADORA
**Herramienta libre para la creación y uso de una incubadora con control de temperatura** 

Con esta herramienta es posible determinar qué temperatura se quiere tener dentro de un espacio cerrado, para mantener un ambiente controlado y monitoreado.
La incubadora se maneja vía bluetooth mediante una App en el móvil que, resumidamente, se conecta con la tecnología Arduino, la cual va a controlar la temperatura enviando órdenes a una fuente de calor que se encuentra dentro. La temperatura y humedad se monitorean a través de un sensor del cual se reciben los datos en la App. 

Este repositorio contiene todos los recursos necesarios para construir y utilizar una incubadora para cultivo, incluyendo el manual de armado y operación, código en Arduino, la aplicación para el control vía Bluetooth y el código base de la aplicación.


Incluye:
1. Armado
2. Operación
3. App y código base
4. Código en Arduino
5. Imágenes
6. Manual de armado

**Armado**  
Se presenta el procedimiento para realizar el armado de la incubadora, diferenciándolo en 2 partes, la estructura con la fuente de calor y el circuito basado en Arduino.

**Operación**  
Se describen los pasos a seguir para poder utilizar la incubadora correctamente, seguido de algunas observaciones y recomendaciones. 

**Aplicación y su código base**<br>
Aquí se encuentra la aplicación móvil que permite controlar la incubadora a través de una conexión Bluetooth. Proporciona la interfaz para configurar la temperatura deseada y monitorear el estado de la incubadora desde un dispositivo móvil. También se encuentra el código fuente de la aplicación móvil que puede ser útil para realizar modificaciones o mejoras en la aplicación.

**Código en Arduino**<br>
Este directorio contiene el código fuente necesario para programar el microcontrolador Arduino que controla la incubadora. Incluye archivos de código (.ino) y las bibliotecas adicionales requeridas.

**Manual de Armado y Operación** <br>
El manual de armado proporciona instrucciones detalladas paso a paso sobre cómo construir la incubadora. Incluye una lista de materiales necesarios y diagramas explicativos.
También se describe cómo operar la incubadora. Cubre aspectos como la configuración, ajuste de la temperatura, monitoreo de la temperatura y humedad y cualquier otra consideración importante.

**Contribuciones**<br>
Las contribuciones son bienvenidas. Si tienes ideas para mejorar la incubadora, correcciones para el código o el manual, o simplemente quieres colaborar de alguna manera.