Aquí se encunetra el código en Arduino que controla el funcionamiento de la Incubadora. Esta sección está dedicada a quienes están interesados en entender en mayor profundidad el funcionamiento y/o quieran realizar mejoras o modificaciones.

Si bien en el código se intenta describir qué es lo que hace cada sección del mismo, a continuación se describe resumidamente y en palabras como es la lógica y el funcionamiento dentro de la Incubadora.

1. Se incluye la librería "DHT.h". Se definen los pin controladores de los elementos y la variable que se va a utilizar durante todo el código, "TemperaturaDeseada".
2. Se da inicio a la comunicación serie y el sensor de T y H. 
3. **Dentro del loop** se lee cada 5 segundos la temperatura y humedad y se lo envían al dispositivo remoto a través de la comuncación serie. Si hay datos disponibles, como una nueva Temperatura Deseada, se leen y actualiza la variable. 
4. **Para el control de la lámpara**, si la temperatura medida es mayor o igual a la temperatura deseada, la lámpara se apaga. Si la temperatura está cerca de la deseada (a menos de un grado), la lámpara entra en un ciclo de encendido y apagado: Se enciende la lámpara por 10 segundos y luego se apaga por 20 segundos. Se vuelve a medir la temperatura, y si sigue siendo menor que la deseada, se repite el ciclo hasta alcanzar la temperatura deseada. Si la temperatura es significativamente menor que la deseada, la lámpara se mantiene encendida.

Las sugerencias de mejora o aportes al código de funcionamiento son bienvenidas. 