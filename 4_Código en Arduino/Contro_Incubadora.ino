// Incluimos librería
#include <DHT.h>

// Definimos el pin digital donde se conecta el sensor de T y H
#define DHTPIN 2
// Dependiendo del tipo de sensor
#define DHTTYPE DHT11

// Pin de control del relé
int relay_pin = 7;

// Inicializamos el sensor DHT11
DHT dht(DHTPIN, DHTTYPE);

// Variables para la comunicación Bluetooth
float TemperaturaDeseada = 30.0; // Temperatura deseada inicial

void setup() {
  // Inicializamos comunicación serie
  Serial.begin(9600);

  // Configuramos el pin del relé como salida
  pinMode(relay_pin, OUTPUT);

  // Inicializamos el sensor DHT
  dht.begin();
}

void loop() {
  // Esperamos 5 segundos entre medidas
  delay(5000);

  // Leemos la temperatura en grados centígrados
  float t = dht.readTemperature();
  float h = dht.readHumidity();
  // Enviamos la temperatura y humedad al dispositivo remoto
  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.println(" °C <br>");
  Serial.print("Humedad:  ");
  Serial.print(h);
  Serial.println(" % ");

  // Si hay datos disponibles en el puerto serie, leemos la temperatura deseada
  if (Serial.available() > 0) {
    // Leemos la cadena de datos recibidos hasta el salto de línea
    String input = Serial.readStringUntil('\n');
    // Convertimos la cadena en un número flotante
    TemperaturaDeseada = input.toFloat();
  }

  // Controlamos la lámpara en base a la temperatura deseada

  // Si la temperatura es mayor a la deseada, mantenemos la lámpara apagada
  if (t >= TemperaturaDeseada) {
    digitalWrite(relay_pin, LOW); // Modo relay Normalmente Abierto (NO)
  } else {
    // Si estamos a un grado o menos de la temperatura deseada, entramos en el ciclo de encendido y apagado
    if (t >= TemperaturaDeseada - 1) {
      // Encendemos la lámpara por 10 segundos
      digitalWrite(relay_pin, HIGH);
      delay(10000);

      // Apagamos la lámpara y esperamos 20 segundos
      digitalWrite(relay_pin, LOW);
      delay(20000);

      // Medimos nuevamente la temperatura
      t = dht.readTemperature();

      // Si hay datos disponibles en el puerto serie, leemos la temperatura deseada
        if (Serial.available() > 0) {
          // Leemos la cadena de datos recibidos hasta el salto de línea
          String input = Serial.readStringUntil('\n');
          // Convertimos la cadena en un número flotante
          TemperaturaDeseada = input.toFloat();
        }
    
      // Si la temperatura sigue siendo menor a la deseada, volvemos a encender la lámpara
      while (t < TemperaturaDeseada) {
        // Encendemos la lámpara por 10 segundos
        digitalWrite(relay_pin, HIGH);
        delay(10000);

        Serial.print("Temperatura: ");
        Serial.print(t);
        Serial.println(" °C <br>");
        Serial.print("Humedad:  ");
        Serial.print(h);
        Serial.println(" % ");

        // Apagamos la lámpara y esperamos 20 segundos
        digitalWrite(relay_pin, LOW);
        delay(10000);

        // Medimos nuevamente la temperatura
        t = dht.readTemperature();
        // Enviamos la temperatura y humedad al dispositivo remoto
        Serial.print("Temperatura: ");
        Serial.print(t);
        Serial.println(" °C <br>");
        Serial.print("Humedad:  ");
        Serial.print(h);
        Serial.println(" % ");
               
        // Si hay datos disponibles en el puerto serie, leemos la temperatura deseada
        if (Serial.available() > 0) {
          // Leemos la cadena de datos recibidos hasta el salto de línea
          String input = Serial.readStringUntil('\n');
          // Convertimos la cadena en un número flotante
          TemperaturaDeseada = input.toFloat();
        }
      }
    } else {
      // Si la temperatura es menor a la deseada - 1, mantenemos la lámpara prendida
      digitalWrite(relay_pin, HIGH);
    }
  }
}
