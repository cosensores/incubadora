# Armado
A continuación se presenta el procedimiento para realizar el armado de la incubadora. Se diferencia en dos partes: por un lado, el armado estructural de la incubadora y el sistema generador de calor y, por otro lado, el armado del circuito basado en Arduino para el control y funcionamiento.

## Estructura y fuente de calor
Para la estructura de la incubadora se utilizó una conservadora común de 15 litros (Fig 1). El volúmen de la incubadora puede modificarse en función de lo que se necesite.

La estructura del soporte donde se apoyan los elementos a incubar debe tener un tamaño específico para que se ajuste al fondo de la conservadora, dejando un espacio para el sistema de calefacción. El soporte debe tener un tamaño necesario para poder apoyar encima los elementos que se van a incubar (p.ej: cajas de petri). Además, deberá dejarse un espacio para que el aire circule.  Se pueden agregar la cantidad de soportes o bases que se necesiten y quepan dentro de la incubadora.

En la figura 2 se puede ver un ejemplo de cómo fue realizado, una estantería en dos niveles. Las rejillas fueron confeccionadas en plástico ABS cortado láser, las uniones impresas 3D y los parantes de varilla plástica.

Otra forma también utilizada fue una estructura de pvc espumada cortada a medida para que quede ubicada por encima del sistema de calentamiento. Este soporte tiene la rigidez necesaria para soportar el peso de los elementos a incubar pero al hacer pruebas con la incubadora funcionando se notaron algunas deformidades. Puede que a la larga no sea una buena opción. 

Respecto al sistema de calentamiento, es posible construirla a partir de diferentes materiales que sirvan como emisores de calor, ya sea una lámpara halógena o una resistencia. Dependiendo del sistema de calentamiento seleccionado, es posible realizar su armado siguiendo las instrucciones del presente manual. En este caso se utiliza un sistema de calentamiento compuesto por dos lámparas halógenas de 5w y dos cooler que hacen circular el calor dentro del compartimiento. Este sistema debe estar dispuesto dentro del contenedor manteniendo distancia de las paredes laterales de la conservadora  (figura 1).

![Incubadora y sistema de calentamiento](/5_Imágenes/Incubadora_y_sistema_de_calentamiento.png)  
Fig. 1: Sistema de calentamiento de la incubadora. Fuente de calor (rojo) y coolers (azul) ubicado en el centro inferior de la conservadora.

![Estantería](/5_Imágenes/Estanteria.png)  
Fig. 2: Ejemplo de la estantería o base apoyada en el fondo de la incubadora sobre la fuente de calor.

Para realizar las conexiones y la instalación del sistema de calefacción y el sensor de temperatura se deben hacer perforaciones en la conservadora. Para el caso de la instalación del sensor de temperatura, se debe hacer una abertura en la tapa de la conservadora, de forma que el sensor quede en la parte superior de la misma (figura 3). Por esta abertura deben pasar los cables para la conexión del sensor. En el caso del sistema de calefacción, debe hacerse una abertura en el costado lateral de la conservadora (figura 1). A través de estas aberturas debe pasar el cableado para la conexión.

![Tapa](/5_Imágenes/Tapa_de_la_Incubadora.png)  
Fig. 3: Abertura en la tapa para pasar los cables controladores del sensor de temperatura y humedad.

## Circuito basado en Arduino
El circuito para el funcionamiento y control de la incubadora fue diseñado utilizando un Arduino como microcontrolador. En la figura 4 se muestra de forma esquemática el circuito con el cableado y los componentes. El Cooler y la placa de Arduino UNO son alimentadas con 12v, ésta última a su vez alimenta con 5v y controla a los módulos del Relé, Bluetooth y Sensor de temperatura. La fuente de calor es alimentada con 220v, siendo controlado su encendido y apagado mediante el Relé. En la figura 5 se puede ver en foto un ejemplo de cómo se realizó haciendo uso de un transformador de 220v a 12v para alimentar a todo el sistema. El transformador y la fuente de calor son alimentados con 220v, del transformador salen 12v para alimentar la placa Arduino y el Cooler, luego, la placa arduino alimenta al resto de componentes con 5v.

![Esquema de cableado](/5_Imágenes/Esquema_del_cableado.png)  
Fig. 4: Esquema del cableado para la incubado

![Foto del Cableado](/5_Imágenes/Foto_del_cableado.png)  
Fig. 5: Cableado por fuera de la incubadora haciendo uso de un transformador para alimentar el circuito.

El código utilizado en la placa arduino para controlar el funcionamiento de la incubadora se puede encontrar en la carpeta [Código en Arduino](https://gitlab.com/cosensores/incubadora/-/tree/main/C%C3%B3digo%20en%20Arduino?ref_type=heads).

## Observaciones y Recomendaciones

#### Cableado del Relé
Como observación para que el código funcione correctamente, al momento de realizar el cableado del relé entre la fuente de calor y la fuente de energía, es importante tener en cuenta las siguientes consideraciones:

El código está hecho teniendo en cuenta que la conexión entre la fuente de calor y el relé está en la configuración de “normalmente abierto”. Ésta configuración puede depender del relé, pero en general está señalizada la entrada del cable como “NO” en el relé. 
Otra opción es normalmente cerrado (NC). Si se elige ésta conexión el sistema de encendido y apagado de la fuente de calor funcionará al revés. Observando la figura 4 se puede ver cómo está cableado entre “COM” y “NO” para lograr la configuración de normalmente abierto. 