# Operación

Para el uso de la incubadora primero se debe tener la App en un teléfono móvil, la cual se puede descargar desde la carpeta [App y código base](https://gitlab.com/cosensores/incubadora/-/tree/main/App%20y%20c%C3%B3digo%20base?ref_type=heads).

Antes de comenzar a utilizar la aplicación se debe establecer conexión por bluetooth con la incubadora siguiendo los siguientes pasos que se muestran en la figura 1.

1. Verificar que la placa HC-06 se encuentre vinculada con el teléfono móvil mediante Bluetooth.
2. Una vez está vinculado el módulo bluetooth con el teléfono, establecer la conexión mediante la App entrando al logo de bluetooth (阝).
3. En la lista que se despliega seleccionar la opción correspondiente a la placa HC-06.

![Pasos para conexión](/5_Imágenes/Pasos_para_establecer_conexión.png)  
Fig. 1: Pasos para establecer conexión bluetooth desde la App.

Una vez hecha la conexión, los datos de temperatura y humedad deberán verse en la parte inferior de la pantalla (figura 2). Éstos valores se renuevan cada 15s, mostrando la temperatura y humedad en el momento de la medición. 
Por defecto la temperatura a la cual se va a establecer el interior de la incubadora está dispuesta en 30°C. Para cambiar la temperatura se deben poner dentro de la casilla central los °C que se desean tener y luego confirmar la elección haciendo click en el botón de “Enviar”. En el ejemplo de la figura 2 se estableció la temperatura a 50°C.

![Ejemplo App](/5_Imágenes/Ejemplo_de_app.png)  
Fig. 2: ejemplo de temperatura establecida en 50°C.

Una vez establecida y enviada la temperatura deseada, la incubadora encenderá o apagará la fuente de calor para lograr llegar a esa temperatura y mantenerla estable. Llegado éste paso ya no es necesario mantener la conexión bluetooth para que la incubadora siga funcionando, sólo va a ser necesario cuando se quiera monitorear la temperatura y humedad interior. Para salir de la App y desconectar el bluetooth, se puede seleccionar la opción de salir o cerrar la aplicación. Cada vez que se quiera volver a establecer la conexión se deben realizar los pasos mencionados anteriormente.

## Observaciones y Recomendaciones

#### Cambios de temperatura 
Cuando se quiera elegir una temperatura distinta a 30°C es recomendable hacerlo una vez prendida la incubadora o, en su defecto, apagar y volver a prenderla para luego modificarla. Es posible que si no se reinicia, la incubadora tarde más en alcanzar la temperatura deseada. 
 
#### Problemas de conexión
Puede que al intentar establecer la conexión bluetooth desde la aplicación, el dispositivo no se encuentre. Ésto se debe a que no se estableció correctamente primero la conexión con el móvil. Primero se debe establecer la conexión bluetooth con el móvil y luego con la aplicación.

#### Envío y recepción de datos
Puede que en ocasiones no se envíen ni se reciban datos desde la app. Ésto puede deberse a que se perdió la conexión bluetooth con la incubadora. Se recomienda volver a establecer la conexión desde la app, desde el símbolo de Bluetooth (阝).

#### Tiempos de prueba
El tiempo máximo sin interrupción que se utilizó la incubadora sin presentar ninguna falla ni inconveniente fue de 48 horas a 35 °C.
